// Saves screenshot as PNG file.
using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.EventSystems;

public class PNGUploader : MonoBehaviour
{
    CursorManager cursorManager;
    public RectTransform marcoGrid;
    public GameObject botonCapt, botonAtras, botonAlante, botonGameOfLife, slider, text, group;

    private void Start() {
        cursorManager = GetComponent<CursorManager>();
    }

    public void BotonCaptura() {
        StartCoroutine(UploadPNG());
    }

    /* Corrutina que guarda una captura del grid en la carpeta Screenshots */
    IEnumerator UploadPNG()
    {
        // We should only read the screen buffer after rendering is complete
        yield return new WaitForEndOfFrame();

        Rect area = marcoGrid.rect;

        // Create a texture the size of the screen, RGB24 format
        int width = (int) (area.width * marcoGrid.localScale.x) + 160;
        int height = (int) (area.height * marcoGrid.localScale.y) + 90;
        //Texture2D tex = new Texture2D(width, height, TextureFormat.RGB24, false);
        Texture2D tex = new Texture2D((int)((float)Screen.width*0.52f), (int)((float)Screen.height*0.93f), TextureFormat.RGB24, false);

        botonCapt.SetActive(false);
        botonAtras.SetActive(false);
        botonAlante.SetActive(false);
        botonGameOfLife.SetActive(false);
        slider.SetActive(false);
        text.SetActive(false);
        group.SetActive(false);
        Cursor.visible = false;

        yield return new WaitForEndOfFrame();

        // Read screen contents into the texture        
        tex.ReadPixels(new Rect((int)((float)Screen.width/5.61f), 5, (int)((float)Screen.width*0.52f), (int)((float)Screen.height*0.93f)), 0, 0);

        tex.Apply();

        // Encode texture into PNG
        byte[] bytes = tex.EncodeToPNG();
        Object.Destroy(tex);

        botonCapt.SetActive(true);
        botonAtras.SetActive(true);
        botonAlante.SetActive(true);
        botonGameOfLife.SetActive(true);
        slider.SetActive(true);
        text.SetActive(true);
        group.SetActive(true);
        Cursor.visible = true;
        cursorManager.actualizaCursor(-1, false);

        // Write to a file in the Screenshots folder
        if (dirCount( new DirectoryInfo(Application.dataPath + "/../Screenshots")) == 0) {
            File.WriteAllBytes(Application.dataPath + "/../Screenshots/Screenshot.png", bytes);
        } else {
            File.WriteAllBytes(Application.dataPath + "/../Screenshots/Screenshot" + dirCount( new DirectoryInfo(Application.dataPath + "/../Screenshots")) + ".png", bytes);
        }
    }
     
    /* Funcion que cuenta el numero de archivos que se encuentran en un directorio */
    public static long dirCount(DirectoryInfo d) {
        long i = 0;
        FileInfo[] fis = d.GetFiles();
        foreach (FileInfo fi in fis) {
            i++;
        }
        return i;
    }
}