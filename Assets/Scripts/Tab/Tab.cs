using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tab : MonoBehaviour
{

    int panelAct;

    public GameObject[] panel1;
    public GameObject[] panel2;
    public GameObject[] panel3;

    public GameObject[][] paneles;

    void Start()
    {
        panelAct = 0;
        paneles = new GameObject[3][];
        paneles[0] = new GameObject[panel1.Length];
        paneles[1] = new GameObject[panel2.Length];
        paneles[2] = new GameObject[panel3.Length];

        for (int i = 0; i < panel1.Length; i++)
        {
            paneles[0][i] = panel1[i];
        }

        for (int i = 0; i < panel2.Length; i++)
        {
            paneles[1][i] = panel2[i];
        }

        for (int i = 0; i < panel3.Length; i++)
        {
            paneles[2][i] = panel3[i];
        }
    }

    public void OcultaBotonesPanel(int n) {

        for (int i = 0; i < paneles[panelAct].Length; i++)
        {
            paneles[panelAct][i].SetActive(false);
        }

        panelAct = n;

        for (int i = 0; i < paneles[panelAct].Length; i++)
        {
            paneles[panelAct][i].SetActive(true);
            if (i == 0) {
                paneles[panelAct][i].transform.parent.transform.SetSiblingIndex(2);
            }
        }
    }

    public int getPanelAct() {
        return panelAct;
    }
}