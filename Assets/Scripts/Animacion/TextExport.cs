using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEditor;
using UnityEngine.UI;
using TMPro;




public class TextExport : MonoBehaviour {

    string pathActual;
    public string pathArchivo;
    private Manager2 mgr2;

    protected FileInfo theSourceFile = null;
    protected StreamReader reader = null;
    protected string text = " ";

	void Start () {
        pathActual = Application.dataPath + "/AnimacionesGuardadas";
        mgr2 = GetComponent<Manager2>();
	}

    /* Funcion que guarda el estado actual del tablero, los colores, las aristas y los nombres de las capas en
        un archivo de texto .txt */
    public void Guardar() {

        // Archivo num n
        if (dirCount(new DirectoryInfo(pathActual)) == 0) {
            pathArchivo = pathActual + "/animacion.txt";
        } else {
            pathArchivo = pathActual + "/animacion" + dirCount(new DirectoryInfo(pathActual)) + ".txt";
        }
        
        // Creamos el archivo
        if (!File.Exists(pathArchivo)) {
            File.WriteAllText(pathArchivo, "");
        }

        // Anadimos el numero de celdas del tablero guardado
        File.AppendAllText(pathArchivo, mgr2.grid.cellNum+"\n");

        // Casillas y colores para todas las capas
        // 0 capa X,Y {X,Y,...}
        // 1 capa color1 {color2,...}
        for (int i = 0; i < mgr2.listaTableros.Count; i++) {
            string s0 = "0 "+i;
            string s1 = "1 "+i;
            for (int x = 0; x < mgr2.grid.cellNum; x++) {
                for (int y = 0; y < mgr2.grid.cellNum; y++) {
                    if (mgr2.listaTableros[i][x][y] == 1) {
                        s0 += (" "+x+","+y);
                        s1 += (" "+colorToHex(mgr2.listaColores[i][x][y]));
                    }
                }
            }
            s0 += "\n";
            s1 += "\n";
            if (s0 != ("0 "+i+"\n")) {
                File.AppendAllText(pathArchivo, s0);
            }
            if (s1 != ("1 "+i+"\n")) {
                File.AppendAllText(pathArchivo, s1);
            }
        }

        // Aristas para todas las capas
        // 2 capa X,Y {X,Y,...}
        for (int i = 0; i < mgr2.listaAristas.Count; i++) {
            string s = "2 "+i;
            for (int j = 0; j < mgr2.listaAristas[i].Count; j++) {
                s += (" "+mgr2.listaAristas[i][j].x+","+mgr2.listaAristas[i][j].y);
            }
            s += "\n";
            if (s != ("2 "+i+"\n")) {
                File.AppendAllText(pathArchivo, s);
            }
        }

        // Nombres de las capas
        // 4 nombre1 {nombre2,...}
        string str = "4";
        for (int i = 0; i < mgr2.contenido.transform.childCount-1; i++) {
            str += (" "+mgr2.contenido.transform.GetChild(i).GetChild(3).GetComponent<InputField>().text);
        }
        str += "\n";
        if (str != ("4\n")) {
            File.AppendAllText(pathArchivo, str);
        }
    }

    /* Funcion complementaria a la anterior que guarda la animacion guardada actual en el archivo .txt del tablero
        sobre el que se ejecuta la animacion */
    public void GuardarAnim () {
        if (!File.Exists(pathArchivo)) {
            EditorUtility.DisplayDialog("Guarda el tablero primero", "¡Debes guardar el tablero primero!", "OK");
            return;
        }

        // Animaciones
        // 3 {capa tipoAnim X,Y origen duracion} {capa2 tipoAnim2 X2,Y2 origen duracion2,...}
        string str = "3 ";
        for (int i = 0; i < mgr2.listaAnimaciones.Count; i++) {
            str += (mgr2.listaAnimaciones[i].capa+" "+
                    mgr2.listaAnimaciones[i].tipoAnim+" "+
                    mgr2.listaAnimaciones[i].valores.x+","+
                    mgr2.listaAnimaciones[i].valores.y+" "+
                    mgr2.listaAnimaciones[i].origen.x+","+
                    mgr2.listaAnimaciones[i].origen.y+" "+
                    mgr2.listaAnimaciones[i].duracion+" ");
        }
        str += "\n";
        if (str != ("3 \n")) {
            File.AppendAllText(pathArchivo, str);
        }
    }
    
    /* Funcion llamada por el boton de cargar la cual abre una ventana para seleccionar el archivo a cargar, y carga
        este posteriormente */
    public void BotonCargar() {
        string path = EditorUtility.OpenFilePanel("Selecciona una animacion", "", "txt");
        Cargar(path);
    }

    /* Funcion que carga un archivo de texto en la ruta especificada, la codificacion de este viene definida en las
        funciones de guardado superiores */
    public void Cargar(string path) {

        if (path.Length != 0) {
            theSourceFile = new FileInfo (path);
            reader = theSourceFile.OpenText();
            int tam = int.Parse(reader.ReadLine());
            if (tam== mgr2.grid.cellNum) {
                mgr2.Start();
                // Hasta el final del archivo
                while(true) {
                    text = reader.ReadLine();
                    if (text != null) {
                        string[] split = text.Split(' ');
                        // Si es de tipo 0, anade el punto en listaTablero y el color de la siguiente linea en listaColores
                        if (split[0] == "0") {
                            // Siguiente linea
                            string text2 = reader.ReadLine();
                            string[] split2 = text2.Split(' ');
                            // Si es una capa superior a la 0
                            if (split[1] != "0") {
                                mgr2.BotonAnadirCapa("New Layer");
                            }
                            for (int i = 2; i < split.Length; i++) {
                                string[] coords = split[i].Split(',');
                                mgr2.listaTableros[int.Parse(split[1])][int.Parse(coords[0])][int.Parse(coords[1])] = 1;
                                mgr2.listaColores[int.Parse(split[1])][int.Parse(coords[0])][int.Parse(coords[1])] = hexToColor(split2[i]);
                                if (i==2) {
                                    mgr2.colorCapa[int.Parse(split[1])] = hexToColor(split2[i]);
                                }
                            }
                        // Si es de tipo 2, anade el arista en listaAristas
                        } else if (split[0] == "2") {
                            for (int i = 2; i < split.Length; i++) {
                                string[] coords = split[i].Split(',');
                                mgr2.listaAristas[int.Parse(split[1])].Add(new Vector2(int.Parse(coords[0]),int.Parse(coords[1])));
                            }
                        // Si es de tipo 3, anade la animacion en listaAnimaciones
                        } else if (split[0] == "3") {
                            for (int i = 1; i < split.Length-5; i+=5) {
                                string[] valores = split[i+2].Split(',');
                                string[] orig = split[i+3].Split(',');
                                mgr2.listaAnimaciones.Add(new Animacion(int.Parse(split[i]),
                                                                        int.Parse(split[i+1]),
                                                                        new Vector2(float.Parse(valores[0]),float.Parse(valores[1])),
                                                                        new Vector2(float.Parse(orig[0]), float.Parse(orig[1])),
                                                                        float.Parse(split[i+4])));
                            }
                        } else if (split[0] == "4") {
                            for (int i = 1; i < split.Length; i++) {
                                if (mgr2.contenido.transform.GetChild(i-1).gameObject.activeSelf) {
                                    mgr2.contenido.transform.GetChild(i-1).GetChild(3).GetComponent<InputField>().text = split[i];
                                }
                            }
                        }

                    } else {
                        break;
                    }
                }
            } else {
                EditorUtility.DisplayDialog("Selecciona tamano valido", "¡Debes seleccionar el tablero de "+tam+ " casillas!", "OK");
            }
        }
        mgr2.actualizaColores();
    }
	
    /* Funcion que cuenta el numero de archivos que se encuentran en un directorio */
    public static long dirCount(DirectoryInfo d) {
        long i = 0;
        FileInfo[] fis = d.GetFiles();
        foreach (FileInfo fi in fis) {
            if (Path.GetExtension(fi.FullName) != ".meta") {
                i++;
            }
        }
        return i;
    }

    /* Funciones auxiliares para convertir de hexadecimal a rgb, y viceversa */
    public string colorToHex(Color32 color) {
         string hex = color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
         return hex;
    }
     
    public Color hexToColor(string hex) {
        hex = hex.Replace ("0x", "");
        hex = hex.Replace ("#", "");
        byte r = byte.Parse(hex.Substring(0,2), System.Globalization.NumberStyles.HexNumber);
        byte g = byte.Parse(hex.Substring(2,2), System.Globalization.NumberStyles.HexNumber);
        byte b = byte.Parse(hex.Substring(4,2), System.Globalization.NumberStyles.HexNumber);
        byte a = 255;
        if(hex.Length == 8){
            a = byte.Parse(hex.Substring(6,2), System.Globalization.NumberStyles.HexNumber);
        }
        return new Color32(r,g,b,a);
     }
}
