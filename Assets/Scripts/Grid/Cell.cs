using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Clase que representa una celda, cuyo atributo es un vector de dimension 2 que indica la posicion de la celda */
public class Cell : MonoBehaviour
{
    public Vector2 pos;
}