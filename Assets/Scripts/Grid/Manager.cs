﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Manager : MonoBehaviour
{
    public GameOfLife gameOfLife;
    public Grid grid;
    public Camera mainCamera;
    public Color colorActual;
    public ColorIndicator c;
    public Manager2 mgr2;

    public int mode;    // modo de click: 0 puntos | 1 lineas
    public int modeLine; // modo de linea: 0 Slope Intercept | 1 DDA | 2 Bresenham

    public bool firstClick = true;
    public bool clickEnTablero = false;
    private Vector2 casilla1;
    private Vector2 casilla2;
    public GameObject[] buttons;

    void Start()
    {
        // Inicializa los atributos
        gameOfLife = GetComponent<GameOfLife>();
        mgr2 = GetComponent<Manager2>();
        mode = 0;
        modeLine = 2;
        actualizaBotones(mode);
        colorActual = new Color(0, 0, 0, 1);
    }

    void Update()
    {
        // Actualiza el color actual con el escogido por la paleta
        colorActual = c.color.ToColor();

        // Obtener posicion puntero
        RaycastHit2D hit;
        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
 
        // Si el puntero colisiona con algo
        if(hit = Physics2D.Raycast(ray.origin, new Vector2(0,0))) {
            if (hit.collider.gameObject.tag == "Celda") {
                // Si esta pulsado el boton izquierdo
                if(Input.GetMouseButton(0)) {
                    if (clickEnTablero) {
                        switch (mode) {
                            case 0:
                                    mgr2.pinta((int)hit.collider.gameObject.GetComponent<Cell>().pos.x, (int)hit.collider.gameObject.GetComponent<Cell>().pos.y, colorActual);
                                    gameOfLife.actualizaEstados(hit.collider.gameObject.GetComponent<Cell>().pos, colorActual);
                                    mgr2.actualizaColores();
                                    grid.GuardarEstadoGrid();
                                break;
                            default:
                                break;
                        }
                    }
                }

                // Si se pulsa el boton izquierdo
                if(Input.GetMouseButtonDown(0)) {
                    clickEnTablero = true;
                    switch (mode) {
                        case 1:   
                            if (firstClick) {
                                casilla1 = hit.collider.gameObject.GetComponent<Cell>().pos;
                                firstClick = false;
                            } else {
                                casilla2 = hit.collider.gameObject.GetComponent<Cell>().pos;
                                switch (modeLine) {
                                    case 0:
                                        dibujaLineaSlopeIntercept(casilla1, casilla2);
                                        break;
                                    
                                    case 1:
                                        dibujaLineaDDA(casilla1, casilla2);
                                        break;

                                    case 2:
                                        dibujaLineaBresenham(casilla1, casilla2, colorActual);
                                        break;

                                        default:
                                            break;
                                    }
                                    if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl)) {
                                        casilla1 = casilla2;
                                    } else {
                                        firstClick = true;
                                    }
                            }
                            break;

                        case 2:
                            floodFill((int)hit.collider.gameObject.GetComponent<Cell>().pos.x, (int)hit.collider.gameObject.GetComponent<Cell>().pos.y, hit.collider.gameObject.GetComponent<Image>().color, colorActual);
                            grid.GuardarEstadoGrid();
                            break;

                        default:
                            break;
                    }
                }

                if(Input.GetMouseButtonUp(0)) {
                    switch (mode) {
                        case 3:
                            if (clickEnTablero) mgr2.setOrigen(hit.collider.gameObject.GetComponent<Cell>().pos);
                            break;

                        default:
                            break;
                    }
                }

                // Si se pulsa el boton derecho
                if(Input.GetMouseButtonDown(1)) {
                    switch (mode) {
                        case 0:
                            hit.collider.gameObject.GetComponent<Image>().color = colorActual;
                            grid.GuardarEstadoGrid();
                            break;
                        case 1:
                            casilla2 = hit.collider.gameObject.GetComponent<Cell>().pos;
                            switch (modeLine) {
                                    case 0:
                                        dibujaLineaSlopeIntercept(casilla1, casilla2);
                                        break;
                                    
                                    case 1:
                                        dibujaLineaDDA(casilla1, casilla2);
                                        break;

                                    case 2:
                                        dibujaLineaBresenham(casilla1, casilla2, colorActual);
                                        break;

                                    default:
                                        break;
                                }
                            break;
                        default:
                            break;
                    }
                }
            } else if (hit.collider.gameObject.tag == "Fondo") {
                
                if(Input.GetMouseButtonDown(0)) {
                    clickEnTablero = true;
                }
            }
        } else {
            if(Input.GetMouseButtonDown(0)) {
                clickEnTablero = false;
            }
        }
    }

    /* Funcion que, dadas dos casillas, calcula la posicion x e y, y el incremento de x e y (Slope Intercept)*/
    private (int, int, int, int) calculaCoords(Vector2 c1, Vector2 c2) {
        int x = (int)c1.x;
        int y = (int)c1.y;
        int dx = (int)c2.x - (int)c1.x;
        int dy = (int)c2.y - (int)c1.y;
        return (x, y, dx, dy);
    }

    /* Funcion que intercambia de posicion la casilla1 y la casilla2 */
    private (Vector2,Vector2) swapCasilla(Vector2 c1, Vector2 c2) {
        return (c2,c1);
    }

    /* Funcion llamada por los botones que seleccionan el modo de pintado */
    public void BotonModo(int modo) {
        if (modo == 3 && mode != 3) {
            mgr2.modeAntiguo = mode;
        }
        mode = modo;
        actualizaBotones(mode);
        firstClick = true;
    }

    /* Funcion llamada por los botones que seleccionan el modo de linea */
    public void BotonModoLinea(int modoLinea) {
        modeLine = modoLinea;
        firstClick = true;
    }

    /* Funcion llamada por el boton de deshacer/rehacer */
    public void BotonDesRehacer(int n) {
        grid.CargarEstadoGrid(n);
    }

    private void dibujaLineaSlopeIntercept(Vector2 casilla1, Vector2 casilla2) {
        (int x, int y, int dx, int dy) = calculaCoords(casilla1, casilla2);
        int swap = 0; // variable auxiliar para contar las veces que hemos intercambiado casillas

        // Si estan en la misma coord X (Lineas verticales)
        if (dx == 0) {
            // Si la linea va de arriba a abajo, swapeamos casillas
            if (dy < 0) {
                (casilla1, casilla2) = swapCasilla(casilla1, casilla2);
                swap++;
                (x, y, dx, dy) = calculaCoords(casilla1, casilla2);
            }
            while(y<=casilla2.y) {
                mgr2.pinta(x, y, colorActual);
                y += 1;
            }
        // Si no estan en la misma coord X (Lineas no verticales)
        } else {
            float m = (float)dy/(float)dx;
            float b = casilla1.y - m*casilla1.x;

            // Si va de dcha a izq, swapeamos casillas
            if (dx < 0) {
                (casilla1, casilla2) = swapCasilla(casilla1, casilla2);
                swap++;
                (x, y, dx, dy) = calculaCoords(casilla1, casilla2);
            }

            // Si la pendiente es mayor que 1 o menor que -1
            if (m > 1 | m < -1) {
                // Si la pendiente es menor que -1, swapeamos casillas
                if (m < -1) {
                    (casilla1, casilla2) = swapCasilla(casilla1, casilla2);
                    swap++;
                    (x, y, dx, dy) = calculaCoords(casilla1, casilla2);
                }
                while(y<=casilla2.y) {
                    mgr2.pinta(x, y, colorActual);
                    y += 1;
                    x = (int)Mathf.Round((y-b)/m);
                }
            // Si la pendiente esta en el intervalo [-1 y 1]
            } else {
                while(x<=casilla2.x) {
                    mgr2.pinta(x, y, colorActual);
                    x += 1;
                    y = (int)Mathf.Round(m*x+b);
                }
            }
        }

        // Si el numero de intercambios es impar, intercambiamos para dejar el estado inicial
        if (swap % 2 == 1) {
            (casilla1, casilla2) = swapCasilla(casilla1, casilla2);
        }

        mgr2.actualizaColores();
        grid.GuardarEstadoGrid();
    }


    private void dibujaLineaDDA(Vector2 casilla1, Vector2 casilla2) {

        // Punto inicial
        float x=casilla1.x+0.5f;
        float y=casilla1.y+0.5f;

        // Diferenciales
        float dx = casilla2.x-casilla1.x;
        float dy = casilla2.y-casilla1.y;

        // Pendiente
        int m = (int)Mathf.Max(Mathf.Abs(dx), Mathf.Abs(dy));
        float dx1 = dx/(float)m;
        float dy1 = dy/(float)m;

        for (int i=0; i<=m; i++) {
            mgr2.pinta((int)Mathf.Floor(x),(int)Mathf.Floor(y), colorActual);
            x=x+dx1;
            y=y+dy1;
        }

        grid.GuardarEstadoGrid();
    }

    public void dibujaLineaBresenham(Vector2 casilla1, Vector2 casilla2, Color color) {

        // Punto inicial
        int x = (int)casilla1.x;
        int y = (int)casilla1.y;

        mgr2.colorCapa[mgr2.capaActual] = color;

        mgr2.listaAristas[mgr2.capaActual].Add(new Vector2(x-1,y-1));
        mgr2.listaAristas[mgr2.capaActual].Add(new Vector2((int)casilla2.x-1,(int)casilla2.y-1));

        // Diferenciales
        int dx = Mathf.Abs((int)casilla2.x-(int)casilla1.x);
        int dy = Mathf.Abs((int)casilla2.y-(int)casilla1.y);

        // Signos
        int signoX = System.Math.Sign((int)casilla2.x-(int)casilla1.x);
        int signoY = System.Math.Sign((int)casilla2.y-(int)casilla1.y);
        bool swap;

        // Si m es mayor que 1, swapeamos
        if (dy > dx) {
            swap = true;
            int temp = dx;
            dx = dy;
            dy = temp;
        } else {
            swap = false;
        }
        int e = 2*dy - dx;
        int a = 2*dy;
        int b = 2*dy - 2*dx;
        mgr2.pinta(x, y, color);
        for (int i=1; i<=dx; i++) {
            if ( e < 0) {
                if (swap) {
                    y += signoY;
                } else {
                    x += signoX;
                }
                e = e + a;
            } else {
                y += signoY;
                x += signoX;
                e += b;
            }
            mgr2.pinta(x, y, color);
        }

        mgr2.actualizaColores();
        grid.GuardarEstadoGrid();
    }

    private void floodFill(int x, int y, Color currColor, Color newColor) {
        // Base cases 
        if (x < 1 || x > grid.cellNum || y < 1 || y > grid.cellNum) 
            return; 
        if (grid.CeldaPosicion(x,y).GetComponent<Image>().color != currColor || grid.CeldaPosicion(x,y).GetComponent<Image>().color == newColor)
            return;
            
        // Replace the color at cell (x, y) 
        mgr2.pinta(x, y, colorActual);
        //grid.CeldaPosicion(x,y).GetComponent<Image>().color = newColor;
        mgr2.actualizaColores();
        gameOfLife.actualizaEstados(new Vector2(x,y), newColor);
    
        // Recursively call for north, east, south and west 
        floodFill(x+1, y, currColor, newColor); 
        floodFill(x-1, y, currColor, newColor); 
        floodFill(x, y+1, currColor, newColor); 
        floodFill(x, y-1, currColor, newColor); 
    } 

    private void pinta(int coordX, int coordY) {
        grid.CeldaPosicion(coordX,coordY).GetComponent<Image>().color = colorActual;
        gameOfLife.actualizaEstados(new Vector2(coordX,coordY), new Color(255f,255f,255f,255f));
    }

    /* Funcion que actualiza el color de los botones segun el seleccionado */
    public void actualizaBotones (int mode) {
        for (int i=0; i<buttons.Length; i++) {
            if (i == mode) {
                buttons[i].GetComponent<Image>().color = new Color32(197, 255, 255, 255);
            } else {
                buttons[i].GetComponent<Image>().color = new Color32(255, 255, 255, 255);
            }
        }
    }
}
