﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Grid : MonoBehaviour
{
    public GameObject celda;

    public int cellNum = 16;

    // 8, 16, 32
    private float[] cellSpacing = new float[] {5f, 3.5f, 0f};

    public int numEstado;
    public int numEstadosMax;
    private int numEstadosUsados;
    private bool atras = false;
    public Color[][][] estadoColores;
    public GameObject[][] grid;
    public Manager manager;

    void Start()
    {
        // Inicializamos los atributos
        CrearGrid();
        numEstado = -1;
        numEstadosUsados = -1;
        numEstadosMax = 100;
        estadoColores = new Color[numEstadosMax][][];
        GuardarEstadoGrid();
    }

    void Update()
    {
        // Actualizamos el tamano del grid segun la resolucion
        float width = GetComponent<RectTransform>().rect.width;
        GetComponent<GridLayoutGroup>().spacing = new Vector2(cellSpacing[(int)Mathf.Log(cellNum, 2)-3], cellSpacing[(int)Mathf.Log(cellNum, 2)-3]);
        Vector2 newSize = new Vector2(width/cellNum-GetComponent<GridLayoutGroup>().spacing.x, width/cellNum-GetComponent<GridLayoutGroup>().spacing.x);
        GetComponent<GridLayoutGroup>().cellSize = newSize;
    }

    /* Funcion que crea un grid segun el numero de celdas (cellNum) */
    public void CrearGrid() {
        grid = new GameObject[cellNum][];

        float width = GetComponent<RectTransform>().rect.width;
        GetComponent<GridLayoutGroup>().spacing = new Vector2(cellSpacing[(int)Mathf.Log(cellNum, 2)-3], cellSpacing[(int)Mathf.Log(cellNum, 2)-3]);
        Vector2 newSize = new Vector2(width/cellNum-GetComponent<GridLayoutGroup>().spacing.x, width/cellNum-GetComponent<GridLayoutGroup>().spacing.x);
        GetComponent<GridLayoutGroup>().cellSize = newSize;
        for (int i=0; i<cellNum; i++) {
            grid[i] = new GameObject[cellNum];
            for (int j=0; j<cellNum; j++) {
                GameObject obj = Instantiate(celda, transform);
                obj.GetComponent<Cell>().pos = new Vector2(j+1,i+1);
                obj.GetComponent<RectTransform>().sizeDelta = newSize;
                obj.GetComponent<BoxCollider2D>().size = newSize;
                obj.GetComponent<Image>().color = new Color(255f,255f,255f,255f);
                obj.SetActive(true);
                grid[i][j] = obj;
            }
        }

    }

    /* Funcion que elimina el grid actual */
    public void EliminarGrid() {
        for (int i=0; i<cellNum; i++) {
            for (int j=0; j<cellNum; j++) {
                Destroy(grid[i][j]);
            }
        }
    }

    /* Funcion llamada por el boton de Limpiar que resetea el estado de la cuadricula a blanco */
    public void LimpiarGrid() {
        GuardarEstadoGrid();
        for (int i=0; i<cellNum; i++) {
            for (int j=0; j<cellNum; j++) {
                manager.gameOfLife.actualizaEstados(new Vector2(i+1,j+1), new Color(255f,255f,255f,255f));
                manager.mgr2.listaTableros[manager.mgr2.capaActual][i][j] = 0;
                manager.mgr2.listaColores[manager.mgr2.capaActual][i][j] = new Color(255f,255f,255f,255f);
            }
        }
        manager.mgr2.listaAristas[manager.mgr2.capaActual].Clear();
        manager.mgr2.actualizaColores();
        manager.firstClick = true;
    }

    /* Funcion utilizada por los checkboxes para cambiar el grid a otro tamano */
    public void CambiarGrid(int n) {
        if (manager.gameOfLife.play) {
            manager.gameOfLife.BotonComenzar();
        }
        estadoColores = new Color[numEstadosMax][][];
        numEstado = 0;
        LimpiarEstadosGrid();
        EliminarGrid();
        numEstado = -1;
        numEstadosUsados = -1;
        cellNum = n;
        CrearGrid();
        manager.gameOfLife.Start();
        manager.firstClick = true;
        manager.mgr2.Start();
    }

    /* Funcion llamada por las acciones para guardar el estado anterior */
    public void GuardarEstadoGrid() {
        numEstado++;
        numEstadosUsados++;
        if (numEstado == numEstadosMax) {
            MoverEstados();
        }
        if (atras) {
            LimpiarEstadosGrid();
            atras = false;
        }

        estadoColores[numEstado] = new Color[cellNum][];
        for (int i=0; i<cellNum; i++) {
            estadoColores[numEstado][i] = new Color[cellNum];
            for (int j=0; j<cellNum; j++) {
                estadoColores[numEstado][i][j] = grid[i][j].GetComponent<Image>().color;
            }
        }
    }

    /* Funcion llamada por los botones de deshacer/rehacer */
    public void CargarEstadoGrid(int n) {
        if (numEstado+n >= 0 && numEstado+n < numEstadosMax && numEstado+n <= numEstadosUsados) {
            for (int i=0; i<cellNum; i++) {
                for (int j=0; j<cellNum; j++) {
                    grid[i][j].GetComponent<Image>().color = estadoColores[numEstado+n][i][j];
                }
            }
            numEstado = numEstado + n;
            atras = true;
        }
    }

    /* Funcion llamada por los checkboxes para resetear el historial de acciones */
    public void LimpiarEstadosGrid() {
        for (int i = numEstado; i < numEstadosUsados; i++)
        {
            estadoColores[i] = null;
        }
        numEstadosUsados = numEstado;
    }

    /* Funcion auxiliar utilizada por GuardarEstadoGrid la cual mueve todos los estados una posicion anterior
        ignorando el primer estado guardado, ya que el historial utiliza listas en vez de una pila */
    public void MoverEstados() {
        for (int x=0; x<numEstadosMax-1; x++) {
            for (int i=0; i<cellNum; i++) {
                for (int j=0; j<cellNum; j++) {
                    estadoColores[x][i][j] = estadoColores[x+1][i][j];
                }
            }
        }
        estadoColores[numEstadosMax-1] = null;
        numEstado--;
        numEstadosUsados--;
    }

    /* Funcion que devuelve el objeto celda que se encuentra en la posicion i-1, j-1 */
    public GameObject CeldaPosicion(int i, int j) {
        return grid[j-1][i-1];
    }
}
