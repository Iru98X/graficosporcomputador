using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Capa : MonoBehaviour
{
    int numCapa;

    public void setCapa(int n) {
        numCapa = n;
    }

    public int getCapa() {
        return numCapa;
    }
}