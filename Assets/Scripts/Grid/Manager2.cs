using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEditor;
using TMPro;

public struct Animacion {
    public int capa;
    public int tipoAnim;
    public Vector2 valores;
    public Vector2 origen;
    public float duracion;

    public Animacion(int c, int t, Vector2 v, Vector2 o, float d) {
        this.capa = c;
        this.tipoAnim = t;
        this.valores = v;
        this.origen = o;
        this.duracion = d;
    }
}

public struct AnimacionFrame {
    public int tipoAnim;
    public Vector2 valores;

    public AnimacionFrame(int t, Vector2 v) {
        this.tipoAnim = t;
        this.valores = v;
    }
}

public class Manager2 : MonoBehaviour
{
    Tab tab;

    public GameObject contenido;
    public GameObject itemN;

    public Grid grid;
    Manager mgr;
    TextExport txtExp;

    public List<int[][]> listaTableros;
    public List<Color32[][]> listaColores;
    public List<List<Vector2>> listaAristas;
    public List<Color32> colorCapa;

    public List<Animacion> listaAnimaciones;
    public float periodoAnim;
    bool guardarAnim = false;

    public int numCapas;
    public int capaActual;

    bool editandoCapa = false;
    bool play = false;
    private Coroutine playCorout;

    public TMP_InputField origenX, origenY;
    public TMP_InputField traslacionX, traslacionY;
    public TMP_InputField escaladoX, escaladoY;
    public TMP_InputField rotacionAlpha;
    public Toggle cw;
    public TMP_InputField cizallaX, cizallaY;
    public TMP_InputField reflexionX, reflexionY;

    private Vector2 origen;
    public int modeAntiguo;
    public TMP_InputField duracionFrameInputField;

    private bool clockWise = true;

    public void Start () {
        System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
        tab = GetComponent<Tab>();
        mgr = GetComponent<Manager>();
        txtExp = GetComponent<TextExport>();

        numCapas = 0;
        capaActual = -1;
        modeAntiguo = 0;
        origen = new Vector2(0f,0f);
        periodoAnim = 1f;

        //Color32[][] colores = new Color32[grid.cellNum][];
        //int[][] tablero = new int[grid.cellNum][];

        /*for (int i=0; i<grid.cellNum; i++) {
            tablero[i] = new int[grid.cellNum];
            colores[i] = new Color32[grid.cellNum];
            for (int j=0; j<grid.cellNum; j++) {
                tablero[i][j] = 0;
            }
        }*/

        foreach (Transform child in contenido.transform) {
            if (child.gameObject.activeSelf) {
                Destroy(child.gameObject);
            }
        }

        listaTableros = new List<int[][]>();
        listaColores = new List<Color32[][]>();
        listaAristas = new List<List<Vector2>>();
        colorCapa = new List<Color32>();
        listaAnimaciones = new List<Animacion>();

        BotonAnadirCapa("Capa 0");

        contenido.transform.GetChild(0).transform.GetChild(0).GetComponent<Image>().color = new Color32(77,83,243,255);
    }

    void Update () {

        if (Input.GetKeyDown(KeyCode.Return) && tab.getPanelAct()==1) {
            BotonAplicar();
        }
    }

    public void BotonSeleccionarCapa() {
        int ind = EventSystem.current.currentSelectedGameObject.transform.GetSiblingIndex();
        // Gris
        contenido.transform.GetChild(numCapas-1 - capaActual).GetChild(0).GetComponent<Image>().color = new Color32(96,96,96,255);
        // Azul
        EventSystem.current.currentSelectedGameObject.transform.GetChild(0).GetComponent<Image>().color = new Color32(77,83,243,255);
        capaActual = numCapas-1 - ind;
    }

    public void BotonAnadirCapa(string texto) {
        // Visual
        GameObject itemNuevo = Instantiate(itemN, itemN.transform.position, itemN.transform.rotation, contenido.transform);
        itemNuevo.transform.SetSiblingIndex(numCapas-1 - capaActual);
        itemNuevo.SetActive(true);
        numCapas++;
        // Gris
        contenido.transform.GetChild(numCapas-1 - capaActual).GetChild(0).GetComponent<Image>().color = new Color32(96,96,96,255);
        capaActual++;
        itemNuevo.transform.GetChild(3).transform.GetChild(0).GetComponent<Text>().text = texto;
        itemNuevo.name = "Capa"+capaActual;

        // Funcionamiento
        int[][] tablero = new int[grid.cellNum][];
        Color32[][] colores = new Color32[grid.cellNum][];

        for (int i=0; i<grid.cellNum; i++) {
            tablero[i] = new int[grid.cellNum];
            colores[i] = new Color32[grid.cellNum];
            for (int j=0; j<grid.cellNum; j++) {
                tablero[i][j] = 0;
            }
        }

        // Si no es la ultima capa, insertamos la lista en la posicion correspondiente
        if (capaActual != numCapas || numCapas != 1) {
            listaTableros.Insert(capaActual, tablero);
            listaColores.Insert(capaActual, colores);
            listaAristas.Insert(capaActual, new List<Vector2>(){});
            colorCapa.Insert(capaActual, new Color32(255,255,255,255));
        // Si no, anadimos al final
        } else {
            listaTableros.Add(tablero);
            listaColores.Add(colores);
            listaAristas.Add(new List<Vector2>(){});
            colorCapa.Add(new Color32(255,255,255,255));
        }

        // Azul
        contenido.transform.GetChild(numCapas-1 - capaActual).GetChild(0).GetComponent<Image>().color = new Color32(77,83,243,255);
    }

    public void BotonMoverCapa(int dir) {
        int ind = EventSystem.current.currentSelectedGameObject.transform.parent.GetSiblingIndex();
        int numCapa = numCapas-1 - ind;

        print(numCapa);

        // Si no es la ultima capa, insertamos las listas en la posicion correspondiente
        if (numCapa+dir>=0 && numCapa+dir < numCapas) {
            int[][] aux1 = listaTableros[numCapa];
            Color32[][] aux2 = listaColores[numCapa];
            List<Vector2> aux3 = listaAristas[numCapa];
            Color32 aux4 = colorCapa[numCapa];

            listaTableros.RemoveAt(numCapa);
            listaColores.RemoveAt(numCapa);
            listaAristas.RemoveAt(numCapa);
            colorCapa.RemoveAt(numCapa);

            listaTableros.Insert(numCapa+dir, aux1);
            listaColores.Insert(numCapa+dir, aux2);
            listaAristas.Insert(numCapa+dir, aux3);
            colorCapa.Insert(numCapa+dir, aux4);
            // Cambia visualmente de posicion la capa
            EventSystem.current.currentSelectedGameObject.transform.parent.transform.SetSiblingIndex(ind-dir);
        // Si no, anadimos al final
        } else {
            print("BORDE");
        }

        actualizaColores();
    }

    public void BotonEditarCapa() {
        GameObject inpObj = EventSystem.current.currentSelectedGameObject.transform.parent.GetChild(3).gameObject;
        InputField inpField = inpObj.GetComponent<InputField>();
        if (!editandoCapa) {
            inpField.interactable = true;
            inpObj.transform.GetChild(1).GetComponent<Text>().text = " ";
            EventSystem.current.SetSelectedGameObject(inpObj);
            editandoCapa = true;
        } else {
            inpField.interactable = false;
            editandoCapa = false;
        }
    }

    public void BotonEliminarCapa() {
        int numCapa = numCapas-1 - EventSystem.current.currentSelectedGameObject.transform.parent.GetSiblingIndex();
        listaTableros.RemoveAt(numCapa);
        listaColores.RemoveAt(numCapa);
        numCapas--;
        capaActual--;
        if (EventSystem.current.currentSelectedGameObject.transform.parent.GetSiblingIndex() == 0 && contenido.transform.childCount > 0) {
            contenido.transform.GetChild(1).GetChild(0).GetComponent<Image>().color = EventSystem.current.currentSelectedGameObject.transform.parent.gameObject.transform.GetChild(0).GetComponent<Image>().color;
        }
        Destroy(EventSystem.current.currentSelectedGameObject.transform.parent.gameObject);
        actualizaColores();
    }

    public void pinta(int x, int y, Color32 color) {
        if (x-1 >= 0 && x-1 < grid.cellNum && y-1 >= 0 && y-1 < grid.cellNum) {
            if (capaActual != -1) {
                listaTableros[capaActual][x-1][y-1] = 1;
                listaColores[capaActual][x-1][y-1] = color;
                colorCapa[capaActual] = color;
            }
        }
    }

    /* Funcion que actualiza el color de cada casilla en funcion del color de la capa superior de cada pixel activo */
    public void actualizaColores() {
        for (int i = 1; i <= grid.cellNum; i++) {
            for (int j = 1; j <= grid.cellNum; j++) {
                // Ponemos la casilla inicialmente blanca
                grid.CeldaPosicion(i,j).GetComponent<Image>().color = new Color32(255,255,255,255);
                for (int k=listaTableros.Count-1; k >= 0; k--) {
                    // Si la casilla esta activada y el color corresp es distinto de null
                    if (listaTableros[k][i-1][j-1] == 1 && !listaColores[k][i-1][j-1].Equals(new Color32(0,0,0,0))) {
                        grid.CeldaPosicion(i,j).GetComponent<Image>().color = listaColores[k][i-1][j-1];
                        break;
                    }
                }
            }
        }
    }

    public void BotonAplicar() {
        
        // Cogemos todos los valores de los campos de texto
        Vector2 refl = new Vector2(float.Parse(reflexionX.text), float.Parse(reflexionY.text));
        Vector2 ciz = new Vector2(float.Parse(cizallaX.text), float.Parse(cizallaY.text));
        Vector2 rot = new Vector2(float.Parse(rotacionAlpha.text),float.Parse(rotacionAlpha.text));
        Vector2 esc = new Vector2(float.Parse(escaladoX.text), float.Parse(escaladoY.text));
        Vector2 tra = new Vector2(float.Parse(traslacionX.text), float.Parse(traslacionY.text));

        // Calculamos el numero de transformaciones
        List<AnimacionFrame> transformaciones = new List<AnimacionFrame>();
        AnimacionFrame a;
        if (refl != new Vector2(0f,0f)) {
            a = new AnimacionFrame(4,refl);
            transformaciones.Add(a);
        }
        if (ciz != new Vector2(0f,0f)) {
            a = new AnimacionFrame(3,ciz);
            transformaciones.Add(a);
        }
        if (rot.x != 0f) {
            a = new AnimacionFrame(2,rot);
            transformaciones.Add(a);
        }
        if (esc != new Vector2(1f,1f)) {
            a = new AnimacionFrame(1,esc);
            transformaciones.Add(a);
        }
        if (tra != new Vector2(0f,0f)) {
            a = new AnimacionFrame(0,tra);
            transformaciones.Add(a);
        }

        // Anadimos las transformaciones a la lista de animaciones
        if (guardarAnim) {
            if (transformaciones.Count == 1) {
                listaAnimaciones.Add(new Animacion(capaActual,transformaciones[0].tipoAnim,transformaciones[0].valores,origen,periodoAnim));
            } else if (transformaciones.Count == 0){
            } else {
                // Si son varias a la vez, establece periodo 0 para todas menos la ultima
                for (int i = 0; i < transformaciones.Count; i++)
                {
                    float p = 0f;
                    if (i == transformaciones.Count-1) {
                        p = periodoAnim;
                    }
                    listaAnimaciones.Add(new Animacion(capaActual,transformaciones[i].tipoAnim,transformaciones[i].valores,origen,p));
                }
            }
        }

        for (int i = 0; i < transformaciones.Count; i++) {

            if (transformaciones[i].tipoAnim == 4) {
                AplicarReflexion(capaActual, ciz, origen);
            } else if (transformaciones[i].tipoAnim == 3) {
                AplicarCizalla(capaActual, ciz, origen);
            } else if (transformaciones[i].tipoAnim == 2) {
                AplicarRotacion(capaActual, rot.x);
            } else if (transformaciones[i].tipoAnim == 1) {
                AplicarEscalado(capaActual, esc, origen);
            } else if (transformaciones[i].tipoAnim == 0) {
                AplicarTraslacion(capaActual, tra);
            }
        }

        actualizaColores();
    }

    /* Se aplica a todos los puntos de la capa actual */
    public void AplicarTraslacion(int capa, Vector2 v) {
        // Creamos la matriz de traslacion
        float[][] transformacion = new float[3][];
        for (int i = 0; i < 3; i++) {
            transformacion[i] = new float[3];
            for (int j = 0; j < 3; j++) {
                if (i == j) {
                    transformacion[i][j] = 1f;
                } else if (i == 0 && j == 2) {
                    transformacion[i][j] = v.x;
                } else if (i == 1 && j == 2) {
                    transformacion[i][j] = v.y;
                } else {
                    transformacion[i][j] = 0f;
                }
            }
        }
        // Calculamos la lista de puntos resultantes de la aplicacion
        List<Vector2> listaAnadirPuntos = new List<Vector2>();
        List<Color32> listaAnadirColor = new List<Color32>();
        for (int i = 0; i < grid.cellNum; i++) {
            for (int j = 0; j < grid.cellNum; j++) {
                if (listaTableros[capa][i][j] == 1 && !listaColores[capa][i][j].Equals(new Color32(255,255,255,255))) {
                    float[] res = ProductoVec1x3Mat3x3(new float[3] {(float)i,(float)j,1f}, transformacion);
                    int resX = (int)res[0];
                    int resY = (int)res[1];
                    listaTableros[capa][i][j] = 0;
                    // Si esta dentro del tablero
                    if (resX < grid.cellNum && resX >= 0 && resY < grid.cellNum && resY >= 0) {
                        for (int k=0; k<listaAristas[capa].Count;k++) {
                            if (listaAristas[capa][k].x == i && listaAristas[capa][k].y == j) {
                                listaAristas[capa][k] = new Vector2(resX, resY);
                            }
                        }
                        listaAnadirPuntos.Add(new Vector2(resX,resY));
                        listaAnadirColor.Add(listaColores[capa][i][j]);
                    }
                    listaColores[capa][i][j] = new Color32(255,255,255,255);
                }
            }
        }
        // Finalmente los anadimos a la capa actual
        for (int i = 0; i < listaAnadirPuntos.Count; i++) {
            listaTableros[capa][(int)listaAnadirPuntos[i].x][(int)listaAnadirPuntos[i].y] = 1;
            listaColores[capa][(int)listaAnadirPuntos[i].x][(int)listaAnadirPuntos[i].y] = listaAnadirColor[i];
        }
    }

    /* Se aplica a todas las aristas de la capa actual */
    public void AplicarEscalado(int capa, Vector2 v, Vector2 o) {
        // Creamos la matriz de escalado
        float[][] transformacion = new float[3][];
        for (int i = 0; i < 3; i++) {
            transformacion[i] = new float[3];
            for (int j = 0; j < 3; j++) {
                if (i == 0 && j == 0) {
                    transformacion[i][j] = v.x;
                } else if (i == 1 && j == 1) {
                    transformacion[i][j] = v.y;
                } else if (i==2 && j==2) {
                    transformacion[i][j] = 1f;
                } else {
                    transformacion[i][j] = 0f;
                }
            }
        }

        
        // Escalamos los vertices que componen las aristas
        List<Vector2> listaAnadir = new List<Vector2>();
        List<Color32> listaAnadirColor = new List<Color32>();

        for (int i = 0; i < listaAristas[capa].Count; i++) {
            Vector2 arista = listaAristas[capa][i];
            float[] res = ProductoVec1x3Mat3x3(new float[3] {(float)(arista.x-o.x),(float)(arista.y-o.y),1f}, transformacion);
            int resX = (int)(res[0]+(int)o.x);
            int resY = (int)(res[1]+(int)o.y);
            print(arista.x+","+arista.y+" -> "+resX+","+resY);

            listaAnadir.Add(new Vector2(resX,resY));
            // Si esta dentro del tablero
            if ((int)arista.x < grid.cellNum && (int)arista.x >= 0 && (int)arista.y < grid.cellNum && (int)arista.y >= 0) {
                
                if (listaColores[capa][(int)arista.y][(int)arista.x].Equals(new Color32(0,0,0,0))) {
                    listaAnadirColor.Add(new Color32(255,255,255,255));
                } else {
                    listaAnadirColor.Add(listaColores[capa][(int)arista.y][(int)arista.x]);
                }
                listaTableros[capa][(int)arista.y][(int)arista.x] = 0;
                listaColores[capa][(int)arista.y][(int)arista.x] = new Color32(255,255,255,255);
            }
        }
        listaAristas[capa].Clear();

        // Limpiamos el resto de la figura (no vertices)
        for (int i = 0; i < grid.cellNum; i++) {
            for (int j = 0; j < grid.cellNum; j++) {
                if (listaTableros[capa][i][j] == 1 && !listaColores[capa][i][j].Equals(new Color32(255,255,255,255))) {
                    listaTableros[capa][i][j] = 0;
                    listaColores[capa][i][j] = new Color32(255,255,255,255);
                }
            }
        }
        // Finalmente los anadimos a la capa actual
        for (int i = 0; i < listaAnadir.Count; i++) {
            int x = (int)listaAnadir[i].x;
            int y = (int)listaAnadir[i].y;

            // Si el resultado esta dentro del tablero, activamos la casilla
            if (x < grid.cellNum && x >= 0 && y < grid.cellNum && y >= 0) {
                listaTableros[capa][y][x] = 1;
            }
            // En los vertices pares
            if (i % 2 == 0) {
                int x2 = (int)listaAnadir[i+1].x;
                int y2 = (int)listaAnadir[i+1].y;

                // Calcula la pos1 del tablero con la proyeccion del punto1
                int dentroX = x;
                int dentroY = y;
                if (x < 0) {
                    dentroX = 0;
                } else if (x > grid.cellNum-1) {
                    dentroX = grid.cellNum;
                }
                if (y < 0) {
                    dentroY = 0;
                } else if (y > grid.cellNum-1) {
                    dentroY = grid.cellNum;
                }

                // Calcula la pos2 del tablero con la proyeccion del punto2
                int dentroX2 = x2;
                int dentroY2 = y2;
                if (x2 < 0) {
                    dentroX2 = 0;
                } else if (x2 > grid.cellNum-1) {
                    dentroX2 = grid.cellNum;
                }
                if (y2 < 0) {
                    dentroY2 = 0;
                } else if (y2 > grid.cellNum-1) {
                    dentroY2 = grid.cellNum;
                }
                
                // Dibujamos la arista escalada hasta la proyeccion del vertice original
                mgr.dibujaLineaBresenham(
                                        new Vector2(dentroX+1, dentroY+1),
                                        new Vector2(dentroX2+1, dentroY2+1),
                                        colorCapa[capa]
                                    );
            }
        }
    }

    /* Se aplica a todos los puntos de la capa actual */
    public void AplicarRotacion(int capa, float a) {
        // Creamos la matriz de rotacion
        float alpha = a;
        if (clockWise) {
            alpha = -alpha;
        }
        float alphaRad = alpha * Mathf.Deg2Rad; // *2pi / 360
        float[][] transformacion = new float[3][];
        for (int i = 0; i < 3; i++) {
            transformacion[i] = new float[3];
            for (int j = 0; j < 3; j++) {
                if (i == 2 && j == 2) {
                    transformacion[i][j] = 1f;
                } else if (i == j) {
                    transformacion[i][j] = Mathf.Cos(alphaRad);
                } else if (i == 1 && j == 0) {
                    transformacion[i][j] = -Mathf.Sin(alphaRad);
                } else if (i == 0 && j == 1) {
                    transformacion[i][j] = Mathf.Sin(alphaRad);
                } else {
                    transformacion[i][j] = 0f;
                }
            }
        }
        // Calculamos la lista de puntos resultantes de la aplicacion
        List<Vector2> listaAnadirPuntos = new List<Vector2>();
        List<Color32> listaAnadirColor = new List<Color32>();
        for (int i = 0; i < grid.cellNum; i++) {
            for (int j = 0; j < grid.cellNum; j++) {
                if (listaTableros[capa][i][j] == 1 && !listaColores[capa][i][j].Equals(new Color32(255,255,255,255))) {
                    float[] res = ProductoVec1x3Mat3x3(new float[3] {(float)i-float.Parse(origenX.text),(float)j-float.Parse(origenY.text),1f}, transformacion);
                    int resX = (int)res[0]+(int)float.Parse(origenX.text);
                    int resY = (int)res[1]+(int)float.Parse(origenY.text);
                    print(i+","+j+" -> "+resX+","+resY);
                    listaTableros[capa][i][j] = 0;
                    if (resX < grid.cellNum && resX >= 0 && resY < grid.cellNum && resY >= 0) {
                        listaAnadirPuntos.Add(new Vector2(resX,resY));
                        listaAnadirColor.Add(listaColores[capa][i][j]);
                    }
                    listaColores[capa][i][j] = new Color32(255,255,255,255);
                }
            }
        }
        // Finalmente los anadimos a la capa actual
        for (int i = 0; i < listaAnadirPuntos.Count; i++) {
            listaTableros[capa][(int)listaAnadirPuntos[i].x][(int)listaAnadirPuntos[i].y] = 1;
            listaColores[capa][(int)listaAnadirPuntos[i].x][(int)listaAnadirPuntos[i].y] = listaAnadirColor[i];
        }
    }

    public void AplicarCizalla(int capa, Vector2 v, Vector2 o) {
        // Creamos la matriz de cizalla
        float[][] transformacion = new float[3][];
        for (int i = 0; i < 3; i++) {
            transformacion[i] = new float[3];
            for (int j = 0; j < 3; j++) {
                if (i == j) {
                    transformacion[i][j] = 1f;
                } else if (i == 1 && j == 0) {
                    transformacion[i][j] = float.Parse(cizallaY.text);
                } else if (i == 0 && j == 1) {
                    transformacion[i][j] = float.Parse(cizallaX.text);
                } else {
                    transformacion[i][j] = 0f;
                }
            }
        }
        // Escalamos los vertices que componen las aristas
        List<Vector2> listaAnadir = new List<Vector2>();
        List<Color32> listaAnadirColor = new List<Color32>();

        for (int i = 0; i < listaAristas[capa].Count; i++) {
            Vector2 arista = listaAristas[capa][i];
            float[] res = ProductoVec1x3Mat3x3(new float[3] {(float)(arista.x-o.x),(float)(arista.y-o.y),1f}, transformacion);
            int resX = (int)(res[0]+(int)o.x);
            int resY = (int)(res[1]+(int)o.y);
            print(arista.x+","+arista.y+" -> "+resX+","+resY);

            listaAnadir.Add(new Vector2(resX,resY));
            // Si esta dentro del tablero
            if ((int)arista.x < grid.cellNum && (int)arista.x >= 0 && (int)arista.y < grid.cellNum && (int)arista.y >= 0) {
                
                if (listaColores[capa][(int)arista.y][(int)arista.x].Equals(new Color32(0,0,0,0))) {
                    listaAnadirColor.Add(new Color32(255,255,255,255));
                } else {
                    listaAnadirColor.Add(listaColores[capa][(int)arista.y][(int)arista.x]);
                }
                listaTableros[capa][(int)arista.y][(int)arista.x] = 0;
                listaColores[capa][(int)arista.y][(int)arista.x] = new Color32(255,255,255,255);
            }
        }
        listaAristas[capa].Clear();

        // Limpiamos el resto de la figura (no vertices)
        for (int i = 0; i < grid.cellNum; i++) {
            for (int j = 0; j < grid.cellNum; j++) {
                if (listaTableros[capa][i][j] == 1 && !listaColores[capa][i][j].Equals(new Color32(255,255,255,255))) {
                    listaTableros[capa][i][j] = 0;
                    listaColores[capa][i][j] = new Color32(255,255,255,255);
                }
            }
        }
        // Finalmente los anadimos a la capa actual
        for (int i = 0; i < listaAnadir.Count; i++) {
            int x = (int)listaAnadir[i].x;
            int y = (int)listaAnadir[i].y;

            // Si el resultado esta dentro del tablero, activamos la casilla
            if (x < grid.cellNum && x >= 0 && y < grid.cellNum && y >= 0) {
                listaTableros[capa][y][x] = 1;
            }
            // En los vertices pares
            if (i % 2 == 0) {
                int x2 = (int)listaAnadir[i+1].x;
                int y2 = (int)listaAnadir[i+1].y;

                // Calcula la pos1 del tablero con la proyeccion del punto1
                int dentroX = x;
                int dentroY = y;
                if (x < 0) {
                    dentroX = 0;
                } else if (x > grid.cellNum-1) {
                    dentroX = grid.cellNum;
                }
                if (y < 0) {
                    dentroY = 0;
                } else if (y > grid.cellNum-1) {
                    dentroY = grid.cellNum;
                }

                // Calcula la pos2 del tablero con la proyeccion del punto2
                int dentroX2 = x2;
                int dentroY2 = y2;
                if (x2 < 0) {
                    dentroX2 = 0;
                } else if (x2 > grid.cellNum-1) {
                    dentroX2 = grid.cellNum;
                }
                if (y2 < 0) {
                    dentroY2 = 0;
                } else if (y2 > grid.cellNum-1) {
                    dentroY2 = grid.cellNum;
                }
                
                // Dibujamos la arista escalada hasta la proyeccion del vertice original
                mgr.dibujaLineaBresenham(
                                        new Vector2(dentroX+1, dentroY+1),
                                        new Vector2(dentroX2+1, dentroY2+1),
                                        colorCapa[capa]
                                    );
            }
        }
    }

    public void AplicarReflexion(int capa, Vector2 v, Vector2 o) {
        // Creamos la matriz de cizalla
        float[][] transformacion = new float[3][] {new float[3]{1f,0,0},
                                                    new float[3]{0,-1f,0},
                                                    new float[3]{0,0,1f}};
        

        // Escalamos los vertices que componen las aristas
        List<Vector2> listaAnadir = new List<Vector2>();
        List<Color32> listaAnadirColor = new List<Color32>();

        for (int i = 0; i < listaAristas[capa].Count; i++) {
            Vector2 arista = listaAristas[capa][i];
            float[] res = ProductoVec1x3Mat3x3(new float[3] {(float)(arista.x-o.x),(float)(arista.y-o.y),1f}, transformacion);
            int resX = (int)(res[0]+(int)o.x);
            int resY = (int)(res[1]+(int)o.y);
            print(arista.x+","+arista.y+" -> "+resX+","+resY);

            listaAnadir.Add(new Vector2(resX,resY));
            // Si esta dentro del tablero
            if ((int)arista.x < grid.cellNum && (int)arista.x >= 0 && (int)arista.y < grid.cellNum && (int)arista.y >= 0) {
                
                if (listaColores[capa][(int)arista.y][(int)arista.x].Equals(new Color32(0,0,0,0))) {
                    listaAnadirColor.Add(new Color32(255,255,255,255));
                } else {
                    listaAnadirColor.Add(listaColores[capa][(int)arista.y][(int)arista.x]);
                }
                listaTableros[capa][(int)arista.y][(int)arista.x] = 0;
                listaColores[capa][(int)arista.y][(int)arista.x] = new Color32(255,255,255,255);
            }
        }
        listaAristas[capa].Clear();

        // Limpiamos el resto de la figura (no vertices)
        for (int i = 0; i < grid.cellNum; i++) {
            for (int j = 0; j < grid.cellNum; j++) {
                if (listaTableros[capa][i][j] == 1 && !listaColores[capa][i][j].Equals(new Color32(255,255,255,255))) {
                    listaTableros[capa][i][j] = 0;
                    listaColores[capa][i][j] = new Color32(255,255,255,255);
                }
            }
        }
        // Finalmente los anadimos a la capa actual
        for (int i = 0; i < listaAnadir.Count; i++) {
            int x = (int)listaAnadir[i].x;
            int y = (int)listaAnadir[i].y;

            // Si el resultado esta dentro del tablero, activamos la casilla
            if (x < grid.cellNum && x >= 0 && y < grid.cellNum && y >= 0) {
                listaTableros[capa][y][x] = 1;
            }
            // En los vertices pares
            if (i % 2 == 0) {
                int x2 = (int)listaAnadir[i+1].x;
                int y2 = (int)listaAnadir[i+1].y;

                // Calcula la pos1 del tablero con la proyeccion del punto1
                int dentroX = x;
                int dentroY = y;
                if (x < 0) {
                    dentroX = 0;
                } else if (x > grid.cellNum-1) {
                    dentroX = grid.cellNum;
                }
                if (y < 0) {
                    dentroY = 0;
                } else if (y > grid.cellNum-1) {
                    dentroY = grid.cellNum;
                }

                // Calcula la pos2 del tablero con la proyeccion del punto2
                int dentroX2 = x2;
                int dentroY2 = y2;
                if (x2 < 0) {
                    dentroX2 = 0;
                } else if (x2 > grid.cellNum-1) {
                    dentroX2 = grid.cellNum;
                }
                if (y2 < 0) {
                    dentroY2 = 0;
                } else if (y2 > grid.cellNum-1) {
                    dentroY2 = grid.cellNum;
                }
                
                // Dibujamos la arista escalada hasta la proyeccion del vertice original
                mgr.dibujaLineaBresenham(
                                        new Vector2(dentroX+1, dentroY+1),
                                        new Vector2(dentroX2+1, dentroY2+1),
                                        colorCapa[capa]
                                    );
            }
        }
    }

    /* Funciones de multiplicacion de matriz-vector */
    public float[] ProductoVec1x3Mat3x3(float[] vector, float[][] matriz) {
        float[] result = new float[3] {0,0,0};

        for (int i = 0; i < result.Length; i++) {
            result[i] = vector[0] * matriz[i][0] + vector[1] * matriz[i][1] + vector[2] * matriz[i][2];
        }

        return result;
    }

    public float[] ProductoVec1x2Mat2x2(float[] vector, float[][] matriz) {
        float[] result = new float[2] {0,0};

        for (int i = 0; i < result.Length; i++) {
            result[i] = vector[0] * matriz[i][0] + vector[1] * matriz[i][1];
        }

        return result;
    }

    /* Funcion llamada desde manager para establecer el origen al aplicar las transformaciones */
    public void setOrigen (Vector2 og) {
        origen = new Vector2(og.x-1f,og.y-1f);
        origenX.text = (og.x-1f).ToString();
        origenY.text = (og.y-1f).ToString();
        mgr.mode = modeAntiguo;
        mgr.actualizaBotones(modeAntiguo);
    }

    /* Funcion llamada cuando se cambia el valor del inputField de duracion del frame */
    public void setPeriodoAnim () {
        periodoAnim = float.Parse(duracionFrameInputField.text);
    }

    /* Funcion llamada por el checkBox de rotacion, el cual establece el sentido (horario/anti-horario) */
    public void cambiarCw() {
        if (clockWise) {
            clockWise = false;
        } else {
            clockWise = true;
        }
    }

    /* Funcion llamada por el checkBox de aplicar, el cual establece si se guarda o no la siguiente transformacion
        en la animacion actual */
    public void cambiarGuardado() {
        if (guardarAnim) {
            guardarAnim = false;
        } else {
            guardarAnim = true;
        }
    }

    /* Funcion llamada por el boton de play que ejecuta la animacion actual */
    public void BotonPlay() {
        if (!play) {
            play = true;
            playCorout = StartCoroutine("PlayAnimation");
        } else {
            StopCoroutine(playCorout);
            txtExp.Cargar(txtExp.pathArchivo);
            play = false;
        }
    }

    /* Corrutina ejecutada al llamar a la funcion BotonPlay superior */
    public IEnumerator PlayAnimation() {
        if (listaAnimaciones.Count == 0) {
            EditorUtility.DisplayDialog("Ninguna animacion", "¡Debes crear una animacion primero!", "OK");
            play=false;
            yield break;
        }
        //this.capa|this.tipoAnim|this.valores|this.origen|this.duracion
        for (int i = 0; i < listaAnimaciones.Count; i++) {
            Animacion a = listaAnimaciones[i];
            print(a.capa+"|"+a.tipoAnim+"|"+a.valores+"|"+a.origen+"|"+a.duracion);
            if (a.tipoAnim == 4) {
                AplicarReflexion(a.capa, a.valores, a.origen);
            } else if (a.tipoAnim == 3) {
                AplicarCizalla(a.capa, a.valores, a.origen);
            } else if (a.tipoAnim == 2) {
                AplicarRotacion(a.capa, a.valores.x);
            } else if (a.tipoAnim == 1) {
                AplicarEscalado(a.capa, a.valores, a.origen);
            } else if (a.tipoAnim == 0) {
                AplicarTraslacion(a.capa, a.valores);
            }
            actualizaColores();
            yield return new WaitForSeconds(a.duracion);
        }
        play = false;
        yield return null;
    }
}