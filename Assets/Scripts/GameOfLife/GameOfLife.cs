using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class GameOfLife : MonoBehaviour {

    public bool play;
    public Grid gridScript;
    private Manager manager;

    public GameObject[][] grid;
    public int[][] estadoActual;
    public int[][] estadoRefrescar;

    private Vector2[] vecinos;

    public Image botonJuegoVida;
    public Slider slider;
    public Text sliderValue;
    private float period;
    private Coroutine coroutine;

    private Color blanco;

    public void Start() {
        play = false;
        period = 0.86f;
        grid = gridScript.grid;
        manager = GetComponent<Manager>();
        blanco = new Color(255f,255f,255f,255f);
        estadoActual = new int[gridScript.cellNum][];
        for (int x=0; x<gridScript.cellNum; x++) {
            estadoActual[x] = new int[gridScript.cellNum];
        }

        estadoRefrescar = new int[gridScript.cellNum][];
        for (int x=0; x<gridScript.cellNum; x++) {
            estadoRefrescar[x] = new int[gridScript.cellNum];
        }
    }

    private int mod(int x, int m) {
        return (x%m + m)%m;
    }

    private bool estaVivaTablero(int x, int y) {
        if (grid[x][y].GetComponent<Image>().color != blanco) return true;
        return false;
    }

    private bool estaViva(int x, int y) {
        if (estadoActual[x][y] == 1) return true;
        return false;
    }

    private bool estaraViva(int x, int y) {
        if (estadoRefrescar[x][y] == 1) return true;
        return false;
    }

    private void muere(int x, int y) {
        estadoRefrescar[x][y] = 0;
    }

    private void revive(int x, int y) {
        estadoRefrescar[x][y] = 1;
    }

    public void BotonComenzar() {
        if (!play) {
            play = true;

            botonJuegoVida.color = new Color32(120, 195, 41, 255);


            coroutine = StartCoroutine(juegoVida());
        } else {
            play = false;

            botonJuegoVida.color = new Color32(255, 88, 88, 255);

            StopCoroutine(coroutine);
        }
    }

    IEnumerator juegoVida() {
        while (true) {
            if (!play) yield break;

            gridScript.GuardarEstadoGrid();

            // Estado actual
            for (int x=0; x<gridScript.cellNum; x++) {
                for (int y=0; y<gridScript.cellNum; y++) {
                    if (estaVivaTablero(x,y)) {
                        estadoActual[x][y] = 1;
                    } else {
                        estadoActual[x][y] = 0;
                    }
                }
            }

            // Clona estado actual
            for (int x=0; x<gridScript.cellNum; x++) {
                for (int y=0; y<gridScript.cellNum; y++) {
                    estadoRefrescar[x][y] = estadoActual[x][y];
                }
            }

            yield return new WaitForSeconds(period);

            for (int x=0; x<gridScript.cellNum; x++) {
                for (int y=0; y<gridScript.cellNum; y++) {
                    vecinos = new Vector2[] {    new Vector2(mod(x-1,gridScript.cellNum),mod(y-1,gridScript.cellNum)),
                                                 new Vector2(   mod(x,gridScript.cellNum),mod(y-1,gridScript.cellNum)),
                                                 new Vector2(   mod(x+1,gridScript.cellNum),mod(y-1,gridScript.cellNum)),
                                                 new Vector2(   mod(x-1,gridScript.cellNum),mod(y,gridScript.cellNum)),
                                                 new Vector2(   mod(x+1,gridScript.cellNum),mod(y,gridScript.cellNum)),
                                                 new Vector2(   mod(x-1,gridScript.cellNum),mod(y+1,gridScript.cellNum)),
                                                 new Vector2(   mod(x,gridScript.cellNum),mod(y+1,gridScript.cellNum)),
                                                 new Vector2(   mod(x+1,gridScript.cellNum),mod(y+1,gridScript.cellNum))
                                                };

                    // Calcula los vecinos vivos
                    int vecinosVivos = 0;
                    foreach (Vector2 vecino in vecinos) {
                        if (estadoActual[(int)vecino.x][(int)vecino.y] == 1) {
                            vecinosVivos++;
                        }
                    }

                    // Regla 1
                    if (!estaViva(x,y) && vecinosVivos == 3) {
                        revive(x,y);
                    }

                    // Regla 2
                    else if (estaViva(x,y) && (vecinosVivos < 2 || vecinosVivos > 3)) {
                        muere(x,y);
                    }

                }
            }

            for (int x=0; x<gridScript.cellNum; x++) {
                for (int y=0; y<gridScript.cellNum; y++) {
                    // Actualiza el grid
                    if (estaraViva(x,y)) {
                        grid[x][y].GetComponent<Image>().color = manager.colorActual;
                        if (manager.colorActual == new Color(1f,1f,1f,255f)) {
                            muere(x,y);
                            grid[x][y].GetComponent<Image>().color = blanco;
                        }

                    } else {
                        grid[x][y].GetComponent<Image>().color = blanco;
                    }
                    // Copia en estado actual el estado a refrescar
                    estadoActual[x][y] = estadoRefrescar[x][y];
                }
            }
            
            yield return null;
        }
    }

    // Actualiza los estados al pintar
    public void actualizaEstados(Vector2 pos, Color c) {
        if (c != new Color(255f,255f,255f,255f)) {
            estadoActual[(int)(pos.y-1)][(int)(pos.x-1)] = 1;
            estadoRefrescar[(int)(pos.y-1)][(int)(pos.x-1)] = 1;
        } else {
            estadoActual[(int)(pos.y-1)][(int)(pos.x-1)] = 0;
            estadoRefrescar[(int)(pos.y-1)][(int)(pos.x-1)] = 0;
        }
    }

    // Funcion que usa el slider para regular la velocidad
    public void changePeriod() {
        period = slider.value;
        sliderValue.text = slider.value.ToString("0.0")+" s";
    }

    public void stopGame() {
        play = false;
    }
}