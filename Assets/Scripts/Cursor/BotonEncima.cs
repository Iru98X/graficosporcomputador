using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Clase auxiliar que permite mostrar dialogos de ayuda sobre los botones */
public class BotonEncima : MonoBehaviour {

    float timer = 0;
    GameObject bocadillo;

    void Start() {
        foreach(Transform child in transform) {
            if(child.tag == "Bocadillo")
                bocadillo = child.gameObject;
        }
    }

    void OnMouseOver() {
        timer += Time.deltaTime;
        if (timer >= 0.5f) {
            bocadillo.SetActive(true);
        }
    }

    void OnMouseExit() {
        timer = 0f;
        bocadillo.SetActive(false);
    }

}