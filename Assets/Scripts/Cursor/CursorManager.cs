﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class CursorManager : MonoBehaviour
{
    int cursorActual;

    //Vector3 mousePosition;
    
    //private Rigidbody2D rb;
    public Texture2D cursorNormal;
    public Texture2D cursorNormalPresionado;

    //Vector2 position = new Vector2(0f, 0f);
    private int numCursores;
    Vector2[] positions;
    Texture2D[][] cursores;

    // Start is called before the first frame update
    void Start()
    {
        numCursores = 1;
        cursorActual = 0;
        //rb = GetComponent<Rigidbody2D>();

        positions = new Vector2[] {
                                    //new Vector2(10f, -0.8f)
                                    new Vector2(3.5f, 3f)
                                    };

        cursores = new Texture2D[numCursores][];
        cursores[0] = new Texture2D[] {
            cursorNormal,
            cursorNormalPresionado
        };

        Cursor.SetCursor(cursores[cursorActual][0], positions[cursorActual], CursorMode.ForceSoftware);
    }

    // Update is called once per frame
    void Update()
    {
        //mousePosition = Input.mousePosition;
        //mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);
        //position = Vector2.Lerp(transform.position, mousePosition+new Vector3(positions[cursorActual].x, positions[cursorActual].y, 0), movSpeed);
    }

    void FixedUpdate() {
        //rb.MovePosition(position);
    }

    public void actualizaCursor(int c, bool presionado) {
        if (c != -1) {
            cursorActual = c;
        }
        if (presionado) {
            Cursor.SetCursor(cursores[cursorActual][1], positions[cursorActual], CursorMode.ForceSoftware);
        } else {
            Cursor.SetCursor(cursores[cursorActual][0], positions[cursorActual], CursorMode.ForceSoftware);
        }
    }
}
